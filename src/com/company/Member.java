package com.company;

import java.util.Calendar;

public class Member extends Person {

    private int memberNumber;
    private int trainerNumber;          //
    private String MembershipStatus;    // "aktiv", "passiv" eller "udmeldt"
    private String swimStatus;          // "konkurrence" eller "motionist"
    private int account;         // Udgør medlemmets saldo;

    public Member(String cpr, String name, String email, int phone, int memberNumber, int trainerNumber,
                  String membershipStatus, String swimStatus, int account) {
        super(cpr, name, email, phone);
        this.memberNumber = memberNumber;
        this.trainerNumber = trainerNumber;
        MembershipStatus = membershipStatus;
        this.swimStatus = swimStatus;
        this.account = account;
    }

    public int getMemberNumber() {
        return memberNumber;
    }

    public int getTrainerNumber() {
        return trainerNumber;
    }

    public String getMembershipStatus() {
        return MembershipStatus;
    }

    public String getSwimStatus() {
        return swimStatus;
    }

    public int getAccount() {
        return account;
    }

    public void setTrainerNumber(int trainerNumber) {
        this.trainerNumber = trainerNumber;
    }

    public void setMembershipStatus(String membershipStatus) {
        MembershipStatus = membershipStatus;
    }

    public void setSwimStatus(String swimStatus) {
        this.swimStatus = swimStatus;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public int getAge(){
        // Get dette år og lav det til en string validateString
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        String yearInString = String.valueOf(year);
        String validateString = "";
        validateString += yearInString.charAt(2);
        validateString += yearInString.charAt(3);

        // Get cpr fra det enkelte medlem og tag de 2 chars der giver fødselsåret og læg i birthYear
        String cpr = getCpr();
        String birthYear ="";
        birthYear += cpr.charAt(4);
        birthYear += cpr.charAt(5);

        // Valider om året fra cpr nummeret er fra 1900 eller 2000
        int validateNumber = Integer.parseInt(validateString);
        int cprYear = Integer.parseInt(birthYear);

        if (validateNumber > cprYear){
            cprYear += 2000;
        } else if (cprYear > validateNumber){
            cprYear += 1900;
        }

        int age = year - cprYear;

        return age;
    }
}