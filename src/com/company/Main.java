package com.company;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws FileNotFoundException, ParseException {
        Control control = new Control();
        Scanner scanner = new Scanner(System.in);
        control.resultList.loadResultList(control);
        try {
            control.memberList.loadMemberList();
        } catch (FileNotFoundException e) {
            System.out.println("Medlemslisten blev ikke indlæst korrekt\n");
        } catch (InputMismatchException f){
            System.out.println("Der opstod en fejl i indlæsningen af medlemslisten\n");
        }

        try {
            control.trainerList.loadMemberList();
        } catch (FileNotFoundException e){
            System.out.println("Trænerlisten blev ikke indlæst korrekt");
        }

        try {
            control.financialTransactionList.loadTransactionList();
        } catch (FileNotFoundException e){
            System.out.println("Den økonomiske oversigt blev ikke indlæst korrekt");
        }

        try {
            control.financialTransactionList.loadSettingsFile();
        } catch (FileNotFoundException e){
            System.out.println("Settings.txt blev ikke fundet");
        }

        System.out.println("Velkommen");
        while (true) {
            int answer = 0;
            startMenu();
            try {
                answer = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e){
                System.out.println("Du skal skrive et tal\n");
            }
            if (answer == 1) {
                if (chairmanMenu(scanner, control)) {
                    break;
                }
            } else if (answer == 2) {
                if (cashierMenu(scanner, control)) {
                    break;
                }
            } else if (answer == 3) {
                if (trainerMenu(scanner, control, 0)) {
                    break;
                }
            } else if (answer == 4) {
                System.out.println("Tak for i dag");
                break;
            }
        }
    }

    public static void startMenu() {
        System.out.println("1. Formand");
        System.out.println("2. Kassér");
        System.out.println("3. Træner");
        System.out.println("4. Afslut");
    }

    public static boolean chairmanMenu(Scanner scanner, Control control) throws FileNotFoundException {
        int answer;
        while (true) {
            System.out.println("1. Opret nyt medlem");
            System.out.println("2. Rediger medlem");
            System.out.println("3. Se alle medlemmer");
            System.out.println("4. Opret ny træner");
            System.out.println("5. Se alle trænere");
            System.out.println("6. Rediger træner");
            System.out.println("7. Skift bruger");
            System.out.println("8. Afslut");
            try {
                answer = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Du skal skrive et tal \n");
            }
        }
        // Opret nyt medlem
        if (answer == 1) {
            String cpr = "";
            String name = "";
            String email = "";
            int phone = 0;
            int trainerNumber = 0;
            String membershipStatus = "udmeldt";
            String swimStatus = "";
            int contingentPaid = 0;

            System.out.println("Skriv fødselsdato eks. 160296");
            cpr = scanner.nextLine();

            System.out.println("Skriv navn");
            name = scanner.nextLine();

            System.out.println("Skriv email");
            email = scanner.nextLine();

            System.out.println("Skriv tlf nummer");
            phone = getIntegerInput();

            System.out.println("Skriv træner ID");
            trainerNumber = getIntegerInput();

            while (true) {
                System.out.println("Vælg medlemsstatus \n 1. Aktiv \n 2. Passiv \n 3. Udmeldt");
                membershipStatus = scanner.nextLine();
                if (membershipStatus.equalsIgnoreCase("1")) {
                    membershipStatus = "aktiv";
                    break;
                } else if (membershipStatus.equalsIgnoreCase("2")) {
                    membershipStatus = "passiv";
                    break;
                } else if (membershipStatus.equalsIgnoreCase("3")) {
                    membershipStatus = "udmeldt";
                    break;
                } else {
                    System.out.println("Dette er ikke en valgmulighed\n");
                }
            }

            while (true) {
                System.out.println("Angiv svømmestatus \n 1. Konkurrence svømmer \n 2. Motionist");
                swimStatus = scanner.nextLine();
                if (swimStatus.equalsIgnoreCase("1")) {
                    swimStatus = "konkurrence";
                    break;
                } else if (swimStatus.equalsIgnoreCase("2")) {
                    swimStatus = "motionist";
                    break;
                } else {
                    System.out.println("Dette er ikke en valgmulighed\n");
                }
            }

                try {
                    control.memberList.createNewMember(cpr, name, email, phone, trainerNumber, membershipStatus,
                            swimStatus, contingentPaid);
                    System.out.println("Medlemmet blev oprettet korrekt\n");
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                }
            chairmanMenu(scanner, control);
        // Rediger medlem
        } else if (answer == 2) {
            int memberNumber;
            String change;
            String newChange;
            String oldValue;
            while (true) {
                System.out.println("Hvilket medlem vil du redigere? (Meldemsnummer)");
                try {
                    memberNumber = Integer.parseInt(scanner.nextLine());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Du skal skrive medlemsnummeret på det medlem du vil redigere\n");
                }
            }
            while (true) {
                System.out.println("Hvad vil du ændre?");
                System.out.println("1. Cpr \n2. Navn \n3. Email \n4. Tlf \n5. Træner \n6. Medlemskab \n7. Svømme status");
                change = scanner.nextLine();
                if (change.equalsIgnoreCase("1")){
                    change = "cpr";
                } else if (change.equalsIgnoreCase("2")){
                    change = "navn";
                } else if (change.equalsIgnoreCase("3")){
                    change = "email";
                } else if (change.equalsIgnoreCase("4")){
                    change = "tlf";
                } else if (change.equalsIgnoreCase("5")){
                    change = "træner";
                } else if (change.equalsIgnoreCase("6")){
                    change = "medlemskab";
                } else if (change.equalsIgnoreCase("7")){
                    change = "svømme status";
                } else {
                    change = "";
                }

                if (change.equalsIgnoreCase("cpr")) {
                    oldValue = control.memberList.findMember(memberNumber).getCpr();
                    break;
                } else if (change.equalsIgnoreCase("navn")) {
                    oldValue = control.memberList.findMember(memberNumber).getName();
                    break;
                } else if (change.equalsIgnoreCase("email")) {
                    oldValue = control.memberList.findMember(memberNumber).getEmail();
                    break;
                } else if (change.equalsIgnoreCase("tlf")) {
                    oldValue = Integer.toString(control.memberList.findMember(memberNumber).getPhone());
                    break;
                } else if (change.equalsIgnoreCase("træner")) {
                    oldValue = Integer.toString(control.memberList.findMember(memberNumber).getTrainerNumber());
                    break;
                } else if (change.equalsIgnoreCase("medlemskab")) {
                    oldValue = control.memberList.findMember(memberNumber).getMembershipStatus();
                    break;
                } else if (change.equalsIgnoreCase("svømme status")) {
                    oldValue = control.memberList.findMember(memberNumber).getSwimStatus();
                    break;
                } else {
                    System.out.println("Dette er ikke et gyldigt felt der kan redigeres\n");
                }
            }
            System.out.println("Hvad skal det ændres til? (Den gamle værdi var \"" + oldValue + "\")");
            newChange = scanner.nextLine();
            control.memberList.editMember(memberNumber, change, newChange);
            chairmanMenu(scanner, control);
        // Vis alle medlemmer
        } else if (answer == 3) {
            control.memberList.showMemberList();
            chairmanMenu(scanner, control);
        // Opret ny træner
        } else if (answer == 4) {
            System.out.println("Skriv cpr");
            String cpr = scanner.nextLine();
            System.out.println("Skriv navn");
            String name = scanner.nextLine();
            System.out.println("Skriv email");
            String email = scanner.nextLine();
            System.out.println("Skriv tlf nummer");
            int phone = Integer.parseInt(scanner.nextLine());

            control.trainerList.createNewTrainer(cpr, name, email, phone);
            System.out.println("Træneren blev oprettet");
            chairmanMenu(scanner, control);
        // Se alle trænere
        } else if (answer == 5) {
            control.trainerList.showAllTrainers();
            chairmanMenu(scanner, control);
        // Rediger træner
        } else if (answer == 6) {
            int trainerNumber;
            String change;
            String newChange;
            while (true) {
                System.out.println("Hvilken træner vil du redigere? (trænernummer)");
                try {
                    trainerNumber = Integer.parseInt(scanner.nextLine());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Du skal skrive trænernummeret på den træner du vil redigere\n");
                }
            }
            System.out.println("Hvad vil du ændre? (CPR, Navn, Email, Tlf, Træner, Medlemskab, Svømme status");
            change = scanner.nextLine();
            System.out.println("Hvad skal det ændres til? (Gammel værdi her)");
            newChange = scanner.nextLine();
            control.trainerList.editTrainer(trainerNumber, change, newChange);
            chairmanMenu(scanner, control);
        // Skift bruger
        } else if (answer == 7) {
        // Afslut
        } else if (answer == 8) {
            return true;
        }

        return false;
    }

    public static boolean cashierMenu(Scanner scanner, Control control) throws FileNotFoundException, ParseException {
        int answer = 1;
        int subanswer = 0;
        SimpleDateFormat sdf = control.financialTransactionList.sdf;
        Date date = new Date();
        String dato = sdf.format(date);

        while (answer!=0) {
            cashierFrontMenu(control, dato);

            answer = Integer.parseInt(scanner.nextLine());

            if(answer==0){
                break;
            }

            if (answer == 301) {
                String message = "";
                System.out.println("Besked på opkrævning: ");
                message = scanner.nextLine();
                control.financialTransactionList.requestPaymentFromEveryone(control.memberList, message);
            } else if (answer == 302) {
                System.out.print("Opkræv kontingent fra [Medlemsnummer]: ");
                subanswer = Integer.parseInt(scanner.nextLine());
                if (subanswer > 0 && subanswer < (control.memberList.memberList.size() + 1)) {
                    Member m = control.memberList.findMember(subanswer);
                    String transactionMessage = "";
                    System.out.println("Besked på opkrævning til " + m.getName() + ": ");
                    transactionMessage = scanner.nextLine();
                    control.financialTransactionList.requestPaymentFromMember(m, date, control.memberList, transactionMessage);
                    System.out.print(m.getName() + " (Medlemsnummer " + m.getMemberNumber() + ") er blevet opkrævet " + control.financialTransactionList.calculatedContingent((m)));
                    System.out.println();
                }

            } else if (answer == 310) {
                System.out.print("Indtast modtagne kontingentbetaling for [Medlemsnummer]; ");
                subanswer = Integer.parseInt(scanner.nextLine());

                if (subanswer > 0 && subanswer < (control.memberList.memberList.size() + 1)) {
                    Member m = control.memberList.findMember(subanswer);
                    System.out.println("Indbetalt beløb for " + m.getMemberNumber() + ": ");
                    int amount = Integer.parseInt(scanner.nextLine());
                    control.financialTransactionList.processReceivedPayment(m, amount, control.memberList);
                    System.out.println(m.getName() + "'s konto (Medlemsnummer " + m.getMemberNumber() + ") er blevet tilskrevet med " + amount + " kr. ");
                    System.out.println("og udgør " + sdf.format(date) + " kr. " + m.getAccount());
                    System.out.println();
                }

            } else if (answer == 311) {
                control.financialTransactionList.processReceivedPaymentFile(control.memberList);
            } else if (answer == 320) {
                System.out.println("Man kan ikke slette en transaktion, men man kan 'ugøre' den med en rettelsestransaktion.");
                System.out.println("Rettelsestransaktioner indeholder teksten:");
                System.out.println("'Rettelsestransaktion: Modregner transaktions-ID XXXX'");
                System.out.print("Hvilken transaktion vil du 'ugøre'? Indtast bogføringsnummer: ");
                subanswer = Integer.parseInt(scanner.nextLine());
                if (control.financialTransactionList.isValidFT(subanswer)) {
                    control.financialTransactionList.undoFinancialTransaction(control.memberList, subanswer);
                }

            } else if (answer == 340) {
                System.out.println("Juniorer betaler lige nu " + control.financialTransactionList.getJuniorPrice() + " om året");
                System.out.print("Hvad skal de betale fremover? ");
                subanswer = Integer.parseInt(scanner.nextLine());
                control.financialTransactionList.setJuniorPrice(subanswer);
                System.out.println("Prisændringen gælder kun fremtidige opkrævninger");
                System.out.println("Junior <18 medlemsskab koster nu " + control.financialTransactionList.getJuniorPrice() + " om året");

            } else if (answer == 341) {
                System.out.println("Almindeligt aktivt medlemsskab (Mellem 18 og 60 år) koster nu " + control.financialTransactionList.getAdultPrice() + " om året");
                System.out.print("Hvad skal de betale fremover? ");
                subanswer = Integer.parseInt(scanner.nextLine());
                control.financialTransactionList.setAdultPrice(subanswer);
                System.out.println("Prisændringen gælder kun fremtidige opkrævninger");
                System.out.println("Almindeligt >18 <60 aktivt medlemsskab koster nu " + control.financialTransactionList.getAdultPrice() + " om året");

            } else if (answer == 342) {
                System.out.println("Seniorer >60 betaler " + control.financialTransactionList.getSeniorDiscount() + "/1.0 af prisen");
                System.out.print("Hvad skal de betale fremover? ['0.75' svarer til 25% rabat] ");
                double subAnswer = Double.parseDouble(scanner.nextLine());
                control.financialTransactionList.setSeniorDiscount(subAnswer);
                System.out.println("Prisændringen gælder kun fremtidige opkrævninger");
                System.out.println("Seniorer >60 betaler nu " + control.financialTransactionList.getSeniorDiscount() + "/1.0 af prisen");

            } else if (answer == 343) {
                System.out.println("Passive medlemmer betaler nu " + control.financialTransactionList.getPassiveMembershipPrice() + " om året");
                System.out.print("Hvad skal de betale fremover? ");
                subanswer = Integer.parseInt(scanner.nextLine());
                control.financialTransactionList.setPassiveMembershipPrice(subanswer);
                System.out.println("Prisændringen gælder kun fremtidige opkrævninger");
                System.out.println("Passive medlemmer betaler nu " + control.financialTransactionList.getPassiveMembershipPrice() + " om året");

            } else if (answer == 1) {
                control.financialTransactionList.printAlleFinancialTransactions(date);

            } else if (answer == 2) {
                for (Member m : control.memberList.memberList) {
                    System.out.println(m.getMemberNumber() + " " + m.getName() + " " + m.getAccount() + " kr.");
                }

            } else if (answer == 3) {
                for (Member m : control.memberList.memberList) {
                    if (m.getAccount() < 0) {
                        System.out.println(m.getName() + " (Medlemsnr. " + m.getMemberNumber() + ") er i restance med kr. " + m.getAccount());
                    }
                }

            } else if (answer == 4) {
                control.financialTransactionList.sortFTList();

            } else if (answer == 10) {
                control.financialTransactionList.clubStatement(control.memberList, control.financialTransactionList);
                System.out.println("Oooops! The function you requested has not been implemented yet...");
            } else if (answer == 0) {

                break;
            }
            control.financialTransactionList.updateSettingsFile();
            cashierMenu(scanner, control);
        }
        return false;
    }

    public static void cashierFrontMenu(Control control, String dato) {
        String menu001 = "1. Se alle transaktioner";
        String menu002 = "2. Se medlemsliste med saldo";
        String menu003 = "3. Rapport over medlemmer i restance";
        String menu004 = "4. Se alle transaktioner - arrangeret efter beløb";
        String menu010 = "10. Note til årsrapporten ";
        String menu301 = "301. Opkræv kontingent (Alle medlemmer)";
        String menu302 = "302. Opkræv kontingent (Enkelt medlem)";
        String menu310 = "310. Indtast indbetaling af kontingent";
        String menu311 = "311. Indlæs indbetalingsfil ";
        String menu320 = "320. Slet enkelt transaktion (brug transaktions-ID)";
        String menu340 = "340. Ændre kontingent for juniorer.";
        String menu341 = "341. Ændre kontingent for almindelige medlemmer";
        String menu342 = "342. Ændre seniorrabat. Seniorer får 25% rabat";
        String menu343 = "343. Ændre prisen for passivt medlemsskab.";
        String juniorPrice = "Junior pris: " + control.financialTransactionList.getJuniorPrice() + ",- Pr. År";
        String adultPrice = "Voksen pris: " + control.financialTransactionList.getAdultPrice() + ",- Pr. År";
        double udregning = control.financialTransactionList.getSeniorDiscount()*control.financialTransactionList.getAdultPrice();
        String seniorPrice = "Senior pris: " + udregning + ",- Pr. År";
        String passivPrice = "Passiv pris: " + control.financialTransactionList.getPassiveMembershipPrice() + ",- Pr. År";
        //String menu350 = "350. Sætte regnskabsår";
        //String menu351 = "351. Ændre regnskabsår. Er lige nu " + vsdf.format(control.financialTransactionList.getBookKeepingYearBegin() + " til " + vsdf.format(control.financialTransactionList.getBookKeepingYearEnd()));

        System.out.printf("" +
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
                "%s\t\t\t\t\t %s\t\t\t\t %s\t\t\t\t\t\t %s\n" +
                "%s\t\t\t\t %s\t\t\t\t\t %s\t\t\t %s\n" +
                "%s\t\t %s\t\t\t\t\t %s\t\t\t\t %s\n" +
                "%s\t\t\t\t\t\t\t\t %s\t %s\t\t\t\t\t %s\n" +
                "0. Tilbage til hovedmenu\n" +
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" +
                "\nTast her: "
                ,menu001,menu301,menu340,juniorPrice
                ,menu002,menu302,menu341,adultPrice
                ,menu003,menu310,menu342,seniorPrice
                ,menu004
                ,menu010,menu320,menu343,passivPrice);

    }

    public static boolean trainerMenu(Scanner scanner, Control control, int answer) throws FileNotFoundException, ParseException {
        System.out.println("1. Top 5 for specifik disciplin");
        System.out.println("2. Indtast resultat");
        System.out.println("3. Find resultat(er)");
        System.out.println("4. Vis tilknyttede medlemmer til bestemt træner");
        System.out.println("5. Afslut");
        if (answer == 0) { answer = Integer.parseInt(scanner.nextLine()); }

        if (answer == 1) {
            while (true) {
                System.out.println("Vælg disciplin");
                System.out.println("1. Crawl");
                System.out.println("2. Rygsvømning");
                System.out.println("3. Butterfly");
                System.out.println("4. Hundesvømning");
                try {
                    answer = Integer.parseInt(scanner.nextLine());
                    break;
                } catch (NumberFormatException e){
                    System.out.println("Du skal skrive et tal");
                }
            }
            int answer1;
            while (true){
                System.out.println("Vælg distance");
                System.out.println("1. 50 meter");
                System.out.println("2. 100 meter");
                System.out.println("3. 200 meter");
                System.out.println("4. 400 meter");
                try {
                    answer1 = Integer.parseInt(scanner.nextLine());
                    break;
                }catch (NumberFormatException b){
                    System.out.println("Du skal skrive et tal");
                }

            }

            System.out.println("\n\nTop 5-liste\n");
            System.out.printf("%-7s %-10s %-30s %-15s %-10s %-15s %-15s\n", "ID", "Medlem", "Dato", "Disciplin", "Distance", "Resultat Tid", "Stævne");
            control.resultList.showTopResultList(control, answer, answer1);
//            control.resultList.showTopResultList(control, 5);
            System.out.println();
            trainerMenu(scanner,control, 0);
            /*if (control.resultList) {
            if (!topResults.equalsIgnoreCase("")) {*/
            /*} else {
                System.out.println("Der er ikke oprettet nogle resultater endnu.");
                System.out.println("Vil du oprette et resultat?");
                System.out.println("0. Nej");
                System.out.println("2. Ja");
                int response = Integer.parseInt(scanner.nextLine());
                trainerMenu(scanner, control, response);
            }*/
        } else if (answer == 2) {
            DateFormat dateFormat = control.financialTransactionList.sdf;


            int memberID;
            String location;
            Date date = null;
            String disciplin = "";
            int distance = 0;
            double resultTime = 0.0;

            System.out.println("Hvilket medlem har opnået resultatet?");
            memberID = Integer.parseInt(scanner.nextLine());

            System.out.println("Hvilket stævne er resultate fra?");
            location = scanner.nextLine();

            System.out.println("Hvilken dato blev resultatet opnået? (eks. 16-02-1996 10:56)");
            try {
                date = dateFormat.parse(scanner.nextLine());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Hvilken disciplin?" +
                    "\n 1. Crawl" +
                    "\n 2. Rygsvømning" +
                    "\n 3. Butterfly" +
                    "\n 4. Hundesvømning");

            boolean run = false;
            int response;
            response = Integer.parseInt(scanner.nextLine());
            while (!run) {
                if (response == 1) {
                    disciplin = "Crawl";
                    run = true;
                } else if (response == 2) {
                    disciplin = "Rygsvømning";
                    run = true;
                } else if (response == 3) {
                    disciplin = "Butterfly";
                    run = true;
                } else if (response == 4) {
                    disciplin = "Hundesvømning";
                    run = true;
                } else {
                    System.out.println("Indtast gyldig discplin" +
                            "\n 1. Crawl" +
                            "\n 2. Rygsvømning" +
                            "\n 3. Butterfly" +
                            "\n 4. Hundesvømning");
                    response = Integer.parseInt(scanner.nextLine());
                }
            }
            System.out.println("Hvilken distance?" +
                    "\n1. 50 meter " +
                    "\n2. 100 meter" +
                    "\n3. 200 meter" +
                    "\n4. 400 meter");
            boolean start = false;
            int response1;
            response1 = Integer.parseInt(scanner.nextLine());
            while (!start) {
                if (response1 == 1) {
                    distance = 50;
                    start = true;
                } else if (response1 == 2) {
                    distance = 100;
                    start = true;
                } else if (response1 == 3) {
                    distance = 200;
                    start = true;
                } else if (response1 == 4) {
                    distance = 400;
                    start = true;
                } else {
                    System.out.println("Ikke gyldig distance" +
                            "\n1. 50 meter " +
                            "\n2. 100 meter" +
                            "\n3. 200 meter" +
                            "\n4. 400 meter");
                    response1 = Integer.parseInt(scanner.nextLine());
                }
                System.out.println("Indtast resultattid (eks 10.56)");
                while (true) {
                    try {
                        resultTime = Double.parseDouble(scanner.nextLine());
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Ikke gyldig resultattid, skriv tiden igen.");
                    }
                }
            }

            control.resultList.createNewResult(memberID, location, date, disciplin, distance, resultTime, control);
            System.out.println("\nResultatet blev oprettet!\n\n");
            trainerMenu(scanner, control, 0);
        } else if (answer == 3) {
            System.out.println("Hvilke(t) resultat(er)? (Skriv medlemsID plus komma, eks. 1,2,3)");
            String input = scanner.nextLine();
            System.out.printf("%-7s %-10s %-30s %-15s %-10s %-15s %-15s\n", "ID", "Medlem", "Dato", "Disciplin", "Distance", "Resultat Tid", "Stævne");
            control.resultList.showSpecificResults(control, input);
            System.out.println();
            trainerMenu(scanner, control, 0);
        } else if (answer == 4) {
            System.out.println("Skriv træner nr.");
            answer = Integer.parseInt(scanner.nextLine());
            control.memberList.connectedMembers(answer);
            System.out.println();
            trainerMenu(scanner, control, 0);
        } else if (answer == 5) {
            return true;
        }
        return false;

    }

    // static method to get the input and validate
    public static int getIntegerInput() {
        int value = -1;
        while (value < 0)
        {
            try
            {
                value = Integer.parseInt(scanner.nextLine());
            }
            catch (NumberFormatException ime)
            {
                System.out.println("Input var ikke et tal, prøv igen.");
            }
        }
        return value;
    }
}