package com.company;

public class Trainer extends Person{
    private int trainerNumber;

    public Trainer(String cpr, String name, String email, int phone, int trainerNumber) {
        super(cpr, name, email, phone);
        this.trainerNumber = trainerNumber;
    }

    public int getTrainerNumber() {
        return trainerNumber;
    }
}