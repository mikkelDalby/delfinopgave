package com.company;

import java.util.Date;

public class Result {

    private int resultID;
    private int memberID;
    private String location;
    private Date date;
    private String disciplin;
    private int distance;
    private double resultTime;

    public int getResultID() { return resultID; }

    public int getMemberID() { return memberID; }

    public String getLocation() { return location; }

    public Date getDate() { return date; }

    public String getDisciplin() { return disciplin; }

    public int getDistance() { return distance; }


    public double getResultTime() { return resultTime; }

    public Result(int resultID, int memberID, String location, Date dato, String disciplin, int distance, double resultTime) {
        this.resultID = resultID;
        this.memberID = memberID;
        this.location = location;
        this.date = dato;
        this.disciplin = disciplin;
        this.distance = distance;
        this.resultTime = resultTime;
    }

}
