package com.company;

public class Person {
    private String cpr;
    private String name;
    private String email;
    private int phone;

    public Person(String cpr, String name, String email, int phone) {
        this.setCpr(cpr);
        this.setName(name);
        this.setEmail(email);
        this.setPhone(phone);
    }

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {

        String oldDate = cpr;

        if(checkDate(cpr)){
        this.cpr = cpr;
        }
        else {
            System.out.println(cpr + " er ikke en korrekt dato!");
            cpr=fixDate(cpr);
            System.out.println("Vi har oprettet medlemmet med fødselsdato " + cpr + " - Du kan ændre dette via [Formand]-[Redigér medlem]...");
            this.cpr = cpr;
        }
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getPhone() {
        return phone;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }

    public boolean checkDate(String date){

        int day;
      int month;

      boolean endResult = true;

      if(date.length()==6) {
          day = Integer.parseInt(date.substring(0, 2));
          if (day < 1 || day > 31)
              endResult = false;
          month = Integer.parseInt(date.substring(2, 4));
          if (month < 1 || month > 12)
              endResult = false;
          }
      else {endResult = false; };

    return endResult;
    }

    public String fixDate(String date){

        if(date.length()==10){
            date = date.substring(0, 6);
            System.out.println("Vi har kun gemt de første 6 cifre i cpr-nummeret " + date);
        }

        if(checkDate(date))
            return date;

        return "091101";
    }
}