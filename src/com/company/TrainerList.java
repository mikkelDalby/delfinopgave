package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class TrainerList {
    ArrayList<Trainer> trainerList = new ArrayList<>();

    public void createNewTrainer(String cpr, String name, String email, int phone) throws FileNotFoundException {
        int trainerNumber = createTrainerNumber();
        trainerList.add(new Trainer(cpr, name, email, phone, trainerNumber));
        updateTrainerList();
    }

    public int createTrainerNumber() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("trainerList.txt"));
        int trainers = 0;
        while (scanner.hasNext()){
            scanner.next();
            scanner.next();
            scanner.next();
            scanner.next();
            scanner.next();
            trainers++;
        }
        trainers++;

        return trainers;
    }

    public void updateTrainerList() throws FileNotFoundException {
        PrintStream printStream = new PrintStream("trainerList.txt");

        for (Trainer i: trainerList){
            printStream.println(i.getCpr() + "\n" + i.getName() + "\n" + i.getEmail() + "\n" + i.getPhone() + "\n" +
            i.getTrainerNumber());
        }
    }

    public void loadMemberList() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("trainerList.txt")).useDelimiter("\n");

        int trainerNumber;
        String cpr;
        String name;
        String email;
        int phone;

        while (scanner.hasNext()){
            name = scanner.next();
            cpr = scanner.next();
            email = scanner.next();
            phone = scanner.nextInt();
            trainerNumber = scanner.nextInt();

            trainerList.add(new Trainer(name, cpr, email, phone, trainerNumber));
        }
    }

    public void showAllTrainers(){
        int count = 0;
        System.out.println("ID\t\t Cpr\t\t Navn\t\t\t\t\t Email\t\t\t\t\t Tlf");
        for (Trainer i: trainerList){
            System.out.printf("%04d\t %-10s\t %-20s\t %-20s\t %-8d\n",
                    i.getTrainerNumber(), i.getCpr(), i.getName(), i.getEmail(), i.getPhone());
            count++;
        }
        if (count == 0){
            System.out.println("Der er ikke oprettet nogle trænere endnu");
        }
        System.out.println();
    }

    public Trainer findTrainer(int trainerNumber){
        Trainer trainer = null;
        for (Trainer i: trainerList){
            if (i.getTrainerNumber() == trainerNumber){
                trainer = i;
            }
        }
        return trainer;
    }

    public void editTrainer(int trainerNumber, String change, String newChange) throws FileNotFoundException {
        Trainer trainer = findTrainer(trainerNumber);
        if (change.equalsIgnoreCase("cpr")){
            trainer.setCpr(newChange);
            updateTrainerList();
        } else if (change.equalsIgnoreCase("navn")){
            trainer.setName(newChange);
            updateTrainerList();
        } else if (change.equalsIgnoreCase("email")){
            trainer.setEmail(newChange);
            updateTrainerList();
        } else if (change.equalsIgnoreCase("tlf")){
            trainer.setPhone(Integer.parseInt(newChange));
            updateTrainerList();
        } else {
            System.out.println("Det var ikke muligt at lave den ønskede ændring\n");
        }
    }

}
