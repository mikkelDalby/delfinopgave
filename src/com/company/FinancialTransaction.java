package com.company;

import java.util.Date;

public class FinancialTransaction {

    private int memberNumber;
    private int financialTransactionID;
    private int amount;
    private Date date;
    private String message;


    public FinancialTransaction(int financialTransactionID, Date dato, int memberNumber, int amount, String messege) {
        this.memberNumber = memberNumber;
        this.financialTransactionID = financialTransactionID;
        this.amount = amount;
        this.date = dato;
        this.message = messege;

    }

    public int getMemberNumber() {
        return memberNumber;
    }

    public int getFinancialTransactionID() {
        return financialTransactionID;
    }

    public int getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }
}



