package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MemberList {
    ArrayList<Member> memberList = new ArrayList<>();

    public void showMemberList(){
        int count = 0;
        System.out.println("ID\t\t Cpr\t\t Navn\t\t\t\t\t Email\t\t\t\t\t Tlf\t\t Træner\t Medlemskab\t Kontingent\t Svømme status");
        for (Member i: memberList){
            if (!i.getMembershipStatus().equalsIgnoreCase("udmeldt")) {
                System.out.printf("%04d\t %-10s\t %-20s\t %-20s\t %-8d\t %-4d\t %-10s\t %-5d\t\t %-10s\n", i.getMemberNumber(), i.getCpr(),
                        i.getName(), i.getEmail(), i.getPhone(), i.getTrainerNumber(), i.getMembershipStatus(),
                        i.getAccount(), i.getSwimStatus());
                count++;
            }
        }
        if (count == 0){
            System.out.println("Der er ingen medlemmer i medlemslisten");
        }
        System.out.println();
    }

    public void createNewMember(String cpr, String name, String email, int phone, int trainerNumber,
                                String membershipStatus, String swimStatus, int contingentPaid) throws FileNotFoundException {
        int memberNumber = createMemberNumber();

        memberList.add(new Member(cpr, name, email, phone, memberNumber, trainerNumber, membershipStatus,
                swimStatus, contingentPaid));
        updateMemberList();
    }

    public void updateMemberList() throws FileNotFoundException {
        PrintStream printStream = new PrintStream("memberList.txt");

        for (Member i: memberList){
            printStream.println(i.getCpr() + "\n" + i.getName() + "\n" + i.getEmail() + "\n" +
                    i.getPhone() + "\n" + i.getMemberNumber() + "\n" + i.getTrainerNumber() + "\n" +
                    i.getMembershipStatus() + "\n" + i.getSwimStatus() + "\n" + i.getAccount());
        }
    }

    public void loadMemberList() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("memberList.txt")).useDelimiter("\n");

        int memberNumber;
        String cpr;
        String name;
        String email;
        int phone;
        int trainerNumber;
        String membershipStatus;
        String swimStatus;
        int contingentPaid;

        while (scanner.hasNext()){
            name = scanner.nextLine();
            cpr = scanner.nextLine();
            email = scanner.nextLine();
            phone = Integer.parseInt(scanner.nextLine());
            memberNumber = Integer.parseInt(scanner.nextLine());
            trainerNumber = Integer.parseInt(scanner.nextLine());
            membershipStatus = scanner.nextLine();
            swimStatus = scanner.nextLine();
            contingentPaid = Integer.parseInt(scanner.nextLine());

            memberList.add(new Member(name, cpr, email, phone, memberNumber, trainerNumber, membershipStatus,
                    swimStatus, contingentPaid));
        }
    }

    public int createMemberNumber() {
        try{
            Scanner scanner = new Scanner(new File("memberList.txt"));
            int members = 0;
            while (scanner.hasNext()){
                scanner.nextLine(); //Linje 1
                scanner.nextLine(); //Linje 2
                scanner.nextLine(); //Linje 3
                scanner.nextLine(); //Linje 4
                scanner.nextLine(); //Linje 5
                scanner.nextLine(); //Linje 6
                scanner.nextLine(); //Linje 7
                scanner.nextLine(); //Linje 8
                scanner.nextLine(); //Linje 9

                members++;
            }
            members++;
            return members;
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            return 1;
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
            return 1;
        }
    }

    public void editMember(int memberNumber, String change, String newChange) throws FileNotFoundException {
        Member member = findMember(memberNumber);
        if (change.equalsIgnoreCase("cpr")){
            member.setCpr(newChange);
            updateMemberList();
        } else if (change.equalsIgnoreCase("navn")){
            member.setName(newChange);
            updateMemberList();
        } else if (change.equalsIgnoreCase("email")){
            member.setEmail(newChange);
            updateMemberList();
        } else if (change.equalsIgnoreCase("tlf")){
            member.setPhone(Integer.parseInt(newChange));
            updateMemberList();
        } else if (change.equalsIgnoreCase("træner")){
            member.setTrainerNumber(Integer.parseInt(newChange));
            updateMemberList();
        } else if (change.equalsIgnoreCase("medlemskab")){
            member.setMembershipStatus(newChange);
            updateMemberList();
        } else if (change.equalsIgnoreCase("svømme status")){
            member.setSwimStatus(newChange);
            updateMemberList();
        } else {
            System.out.println("Det var ikke muligt at lave den ønskede ændring\n");
        }
    }

    public Member findMember(int memberNumber){
        Member member = null;
        for (Member i: memberList){
            if (i.getMemberNumber() == memberNumber){
                member = i;
            }
        }
        return member;
    }

    public int numberOfPeopleWhoHaveNotPaid(){
        int nopwhnp = 0;

        for(Member m: memberList){
            if(m.getAccount() < 0)
                nopwhnp++;
        }
        return nopwhnp;
    }

    public int numberOfActiveMembers(){
        int noam = 0;

        for(Member m: memberList)
            if(m.getMembershipStatus().equalsIgnoreCase("aktiv"))
                noam++;
        return noam;
    }

    public int numberOfPassiveMembers(){
        int nopm = 0;

        for (Member m: memberList) {
            if (m.getMembershipStatus().equalsIgnoreCase("passiv")){
                nopm++;
        }}
        return nopm;
    }

    public int numberOfUdmeldteMembers(){
        int noum = 0;

        for(Member m: memberList)
            if(m.getMembershipStatus().equalsIgnoreCase("udmeldt"))
                noum++;

        return noum;
    }

    public int numberOfActiveMotionister(){
        int noam = 0;

        for (Member m: memberList) {
            if (m.getMembershipStatus().equalsIgnoreCase("aktiv")){
                if (m.getSwimStatus().equalsIgnoreCase("motionist"))
                    noam++;
        }}
        return noam;
    }

    public int numberOfActiveCompetitiveSwimmers(){
        int noaks = 0;

        for (Member m: memberList) {
            if (m.getMembershipStatus().equalsIgnoreCase("aktiv")){
                if (m.getSwimStatus().equalsIgnoreCase("konkurrence")){
                    noaks++;
        }}}
        return noaks;
    }

    public void connectedMembers(int trainerNumber){
        int count = 0;
        System.out.println("ID\t\t Cpr\t\t Navn\t\t\t\t\t Email\t\t\t\t\t Tlf\t\t Træner\t Medlemskab\t Kontingent\t Svømme status");
        for (Member i: memberList){
            if (i.getTrainerNumber() == trainerNumber && i.getMembershipStatus() != "udmeldt"){
                System.out.printf("%04d\t %-10s\t %-20s\t %-20s\t %-8d\t %-4d\t %-10s\t %-5d\t\t %-10s\n", i.getMemberNumber(), i.getCpr(),
                        i.getName(), i.getEmail(), i.getPhone(), i.getTrainerNumber(), i.getMembershipStatus(),
                        i.getAccount(), i.getSwimStatus());
                count++;
            }
        }
        if (count == 0){
            System.out.println("Der er ingen medlemmer tilknyttet denne træner");
        }
        System.out.println();
    }
}