package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FinancialTransactionList {


    ArrayList<FinancialTransaction> financialTransactionList = new ArrayList<>();

    public SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

    // Anatomy of a financial transaction
    // [FinancialTransactionID] [Date] [MemberNumber] [Amount] [Message]
    // Amount is NEGATIVE when payment is requested and POSITIVE when member pays.
    // Message example "Kontingent 2018"

    // Price list
    private int juniorPrice = 1000;
    private int adultPrice = 2000;
    private int passiveMembershipPrice = 500;
    private double seniorDiscount = 0.75;  // Seniors only pay 75% of the price = 25% discount.
    // Financial key info
    private int financialTransactionID; // Book keeping number - each transaction receives a unique number.
    private Date bookKeepingYearBegin; //Swim clubs "regnskabsår"
    private Date bookKeepingYearEnd;

    /*
        GETTERS OG SETTERS
     */

    public int getJuniorPrice() {
        return juniorPrice;
    }

    public void setJuniorPrice(int juniorPrice) {
        this.juniorPrice = juniorPrice;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getPassiveMembershipPrice() {
        return passiveMembershipPrice;
    }
    // The next number is size()+1.

    public void setPassiveMembershipPrice(int passiveMembershipPrice) {
        this.passiveMembershipPrice = passiveMembershipPrice;
    }

    public double getSeniorDiscount() {
        return seniorDiscount;
    }

    public void setSeniorDiscount(double seniorDiscount) {
        this.seniorDiscount = seniorDiscount;
    }

    public Date getBookKeepingYearBegin() {
        return bookKeepingYearBegin;
    }

    public void setBookKeepingYearBegin(Date bookKeepingYearBegin) {
        this.bookKeepingYearBegin = bookKeepingYearBegin;
    }

    public Date getBookKeepingYearEnd() {
        return bookKeepingYearEnd;
    }

    public void setBookKeepingYearEnd(Date bookKeepingYearEnd) {
        this.bookKeepingYearEnd = bookKeepingYearEnd;
    }


    /*
        METHODS
     */

    // Opkræv kontingent fra samtlige medlemmer på medlemslisten (som ikke er udmeldt)
    public void requestPaymentFromEveryone(MemberList memberList, String message) throws FileNotFoundException {
        Date date = new Date();
        for(Member member : memberList.memberList){
            if (!member.getMembershipStatus().equalsIgnoreCase("udmeldt")){
            requestPaymentFromMember(member, date, memberList, message);
            }
        }
    }

    // Opkræv kontingent fra individuelt medlem
    public void requestPaymentFromMember(Member member, Date date, MemberList memberList, String message) throws FileNotFoundException {
        if (!member.getMembershipStatus().equalsIgnoreCase("udmeldt")) {
            member.setAccount(member.getAccount() - calculatedContingent(member));  // Withdraws contingent from member account.
            memberList.updateMemberList();
            financialTransactionID = financialTransactionList.size() + 1;

            // Kontingentet er f.eks. "2000" kr., så opkrævningstransaktionen bliver lavet med "-2000".
            updateTransactionListWithTransaction(new FinancialTransaction(financialTransactionID, date, member.getMemberNumber(), -calculatedContingent(member), message));
        }
    }

    // I tilfælde af fejlagtigt oprettede økonomiske transaktioner kan disse "ugøres" med en modpostering.
    public FinancialTransaction undoFinancialTransaction (MemberList memberList, int undoNumber) throws FileNotFoundException {
        FinancialTransaction undoFT = null;

        if(isValidFT(undoNumber)) {

            int amount = findFinancialTransaction(undoNumber).getAmount();
            Date date = new Date();

            Member member = memberList.findMember(findFinancialTransaction(undoNumber).getMemberNumber());

            String message = "Rettelsestransaktion: Modregner bogføringsnummer "
                    + undoNumber
                    + " på medlem "
                    + member.getMemberNumber()
                    + " med teksten "
                    + findFinancialTransaction(undoNumber).getMessage();

            undoFT = new FinancialTransaction(financialTransactionList.size() + 1, date, member.getMemberNumber(), -amount, message);

            member.setAccount(member.getAccount() - amount);
            memberList.updateMemberList();

            System.out.println("Bilag med bogføringsnummer " + undoFT.getFinancialTransactionID() + " er bogført med følgende data:");
            printFinancialTransactionInfo(undoFT);

        }
        return undoFT;
    }

    // Tjekker om et bogføringsnummer eksisterer eller ej
    public boolean isValidFT(int number){
        if(number>0 && number<financialTransactionList.size())
            return true;

        return false;
    }

    // Udskriver data fra enkelt økonomisk transaktion
    public void printFinancialTransactionInfo (FinancialTransaction ft){
        System.out.printf("%d\t\t\t\t\t%s\t\t%d\t\t\t\t%d\t%s\n",ft.getFinancialTransactionID(), sdf.format(ft.getDate()), ft.getMemberNumber(), ft.getAmount(),ft.getMessage());
    }

    // Udskriver data fra samtlige økonomiske transaktioner
    public void printAlleFinancialTransactions(Date date){
        System.out.println("Rapport over samtlige økonomiske transaktioner i svømmeklubben Delfinen");
        System.out.println("Udskrevet " + sdf.format(date));
        System.out.println();
        System.out.printf("%s\n%s\t%s\t\t\t\t%s\t%s\t%s\n", "[Info: Negative beløb opkræves medlemmet, positive beløb betales af medlemmet]","Bogføringsnummer","Dato/tid", "Medlemsnummer", "Beløb", "Besked");
        for (FinancialTransaction f: financialTransactionList) {
            printFinancialTransactionInfo(f);
        }
        System.out.println("Slut på Rapport\n");
    }

    // Finder og returnerer økonomisk transaktion
    public FinancialTransaction findFinancialTransaction (int ftNumber){
        FinancialTransaction ftFound = null;
        for(FinancialTransaction ft : financialTransactionList){
            if(ft.getFinancialTransactionID()==ftNumber){
                ftFound = ft;
            }
        }
        if(ftFound==null)
            System.out.println(ftNumber + " findes ikke");
        return ftFound;
    }

    // Tilføjer transaktion til listen og opdaterer financialTransactionList.txt
    public void updateTransactionListWithTransaction(FinancialTransaction financialTransaction) throws FileNotFoundException {
        PrintStream printStream = new PrintStream("financialTransactionList.txt");

        String dato;
        financialTransactionList.add(financialTransaction);

        for(FinancialTransaction ft : financialTransactionList){
            dato = sdf.format(financialTransaction.getDate());
            printStream.println(ft.getFinancialTransactionID() + "\n" + dato + "\n" + ft.getMemberNumber() + "\n" +
                                ft.getAmount() + "\n" + ft.getMessage());
        }
    }

    // Indlæser data fra financialTransactionList.txt og tilføjer data til arbejdsliste
    public void loadTransactionList() throws FileNotFoundException, ParseException {
        Scanner scanner = new Scanner(new File("financialTransactionList.txt"));

        String dato;
        String dato2;
        int ftNumber;
        int memberNumber;
        Date date;
        int amount;
        String message;

        while (scanner.hasNext()){
            ftNumber = Integer.parseInt(scanner.next());
            dato = scanner.next() + " " + scanner.next();
            memberNumber = Integer.parseInt(scanner.next());
            amount = Integer.parseInt(scanner.next());
            message = scanner.next(); // Læser kun det første frem til mellemrum.
            scanner.nextLine();
            date = sdf.parse(dato);

            financialTransactionList.add(new FinancialTransaction(ftNumber, date, memberNumber, amount, message));
        }
    }

    // Udregner hvad specifik medlem skal betale i kontingent
    public int calculatedContingent(Member member){

        int individualPrice = 0;
        if (!member.getMembershipStatus().equalsIgnoreCase("udmeldt")){
            if(member.getMembershipStatus().equalsIgnoreCase("passiv")){
                individualPrice = passiveMembershipPrice;
            }
            else if(member.getMembershipStatus().equalsIgnoreCase("aktiv")) {
                if (member.getAge() < 18) { // JuniorPrice
                    individualPrice = juniorPrice;
                }
                else if (member.getAge() > 17) {
                    individualPrice = adultPrice;

                }
            }

        if (member.getAge() > 60) { // SeniorDiscount;
            individualPrice = (int)(adultPrice * seniorDiscount);
        }}
        return individualPrice;
    }

    //

    // Sammentæller hvor mange penge medlemmer skylder i kontingent i alt
    public int amountOfMoneyNotYetPaid(MemberList memberList){
        int aomnyp = 0;

        for(Member m: memberList.memberList){
            if(m.getAccount() < 0)
                aomnyp += m.getAccount();
        }
        return aomnyp;
    }

    // Udskriver status over svømmeklubbens medlemmer samt disses eventuelle totale restance
    public void clubStatement(MemberList memberList, FinancialTransactionList financialTransactionList){

        int noOfPayingMembers = memberList.numberOfActiveMembers() + memberList.numberOfPassiveMembers();

        System.out.println("Statusrapport:");
        System.out.println("Svømmeklubben Delfin har " + noOfPayingMembers + " betalende medlemmer.");
        System.out.println("Heraf " + memberList.numberOfActiveMembers() + " aktive medlemmer, og " + memberList.numberOfPassiveMembers() + " passive medlemmer.");
        System.out.println("Af de aktive medlemmer er " + memberList.numberOfActiveCompetitiveSwimmers() + " konkurrencesvømmere, og " + memberList.numberOfActiveMotionister() + " motionister");
        System.out.println("Af de " + noOfPayingMembers + " betalende medlemmer mangler " + memberList.numberOfPeopleWhoHaveNotPaid() + " medlemmer at betale i alt kr. " + amountOfMoneyNotYetPaid(memberList) + " kr. i kontingent.");
        System.out.println();
    }


    // Indlæser og håndterer betalinger fra mange medlemmer fra betalingsfil indbetalinger.txt
    public void processReceivedPaymentFile(MemberList memberList) throws FileNotFoundException {
        String filename = "indbetalinger.txt";
        System.out.println("Indlæs fil med betalinger: indbetalinger.txt");
        System.out.println("Bemærk at formatet skal være");
        System.out.println("[Medlemsnummer][Ny linje][Beløb][Ny linje][Medlemsnummer][Ny linje][Beløb][Ny linje]...");
        Scanner scanner = new Scanner(new File(filename));

        int memberID;
        int amount;
        int ft;
        int oldAccount;

        System.out.println("Kvittering over indbetalinger fra indlæsning af fil");
        System.out.println("Kopier listen, hvis du vil lave noget om.");
        System.out.println("[Bogføringsnummer][Medlemsnummer][Navn][Beløb]");
        while(scanner.hasNext()){
            memberID = Integer.parseInt(scanner.next());
            oldAccount = memberList.findMember(memberID).getAccount();
            amount = Integer.parseInt(scanner.next());
            ft = processReceivedPayment(memberList.findMember(memberID), amount, memberList);
            System.out.print("[" + ft + "][" + memberID + "][" + memberList.findMember(memberID).getName() + "][" + amount + "]");
            System.out.println("\t\tGml Saldo: " + oldAccount + " - Ny Saldo:" + memberList.findMember(memberID).getAccount());
        }
    }

    // Modtager og håndterer kontingentbetaling fra enkelt medlem
    public int processReceivedPayment(Member member, int receivedPayment, MemberList memberList) throws FileNotFoundException {

        int ftNumber = financialTransactionList.size()+1;
        Date date = new Date();

        FinancialTransaction ft = new FinancialTransaction(ftNumber, date, member.getMemberNumber(), receivedPayment, "Indbetalt kontant");

        member.setAccount(member.getAccount() + receivedPayment);  // Adds paid contingent to member account.
        memberList.updateMemberList();
        updateTransactionListWithTransaction(ft);

        return ftNumber;
    }


    //OBS: bookKeepingYear bør være "01/01-31/12", dvs. kun dag og måned, og ikke en rigtig dato...
    // Bør også skrives til filen som String og ikke Date.
    public void updateSettingsFile() throws FileNotFoundException {
        PrintStream printStream = new PrintStream("settings.txt");

        printStream.println(juniorPrice + "\n" + adultPrice + "\n" + passiveMembershipPrice + "\n" + seniorDiscount
                + "\n" + financialTransactionID + "\n" + bookKeepingYearBegin + "\n" + bookKeepingYearEnd);
    }

    // Indlæser svømmeklubbens prisliste og størrelsen på seniorrabatten
    public void loadSettingsFile() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("settings.txt"));

        while (scanner.hasNext()){
            juniorPrice = Integer.parseInt(scanner.next());
            adultPrice = Integer.parseInt(scanner.next());
            passiveMembershipPrice = Integer.parseInt(scanner.next());
            seniorDiscount = Double.parseDouble(scanner.next());
            financialTransactionID = Integer.parseInt(scanner.next());
            // Date
            try {
                bookKeepingYearBegin = sdf.parse(scanner.next());
            } catch (ParseException e){
                bookKeepingYearBegin = null;
            }
            // Date
            try {
                bookKeepingYearEnd = sdf.parse(scanner.next());
            } catch (ParseException e){
                bookKeepingYearEnd = null;
            }
        }
    }

    /** Sorterer listen med financielle transaktioner - Dummy funktionalitet for at lege med sortering. */
    public void sortFTList(){

        ArrayList<FinancialTransaction> tempTransactionList = new ArrayList<>();
        for (FinancialTransaction i: financialTransactionList) {
            tempTransactionList.add(findFinancialTransaction(i.getFinancialTransactionID()));
        }
        Collections.sort(tempTransactionList, new Comparator<FinancialTransaction>() {
            @Override
            public int compare(FinancialTransaction o1, FinancialTransaction o2) {
                return o2.getAmount()-o1.getAmount();
            }
        });

        printSortedTransactionList(tempTransactionList);
    }

    /** Udskriver listen med financielle transaktioner efter sortering - Dummy funktionalitet for at lege med sortering. */
    public void printSortedTransactionList(ArrayList<FinancialTransaction> sortedList){
        System.out.println("Rapport over samtlige økonomiske transaktioner i svømmeklubben Delfinen, arrangeret efter beløb");
        System.out.printf("%s\n%s\t%s\t\t\t\t%s\t%s\t%s\n", "[Info: Negative beløb opkræves medlemmet, positive beløb betales af medlemmet]","Bogføringsnummer","Dato/tid", "Medlemsnummer", "Beløb", "Besked");
        for (FinancialTransaction f: sortedList) {
            printFinancialTransactionInfo(f);
        }
        System.out.println("Slut på Rapport\n");
    }



}