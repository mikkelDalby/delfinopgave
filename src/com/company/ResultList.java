package com.company;

import com.sun.org.apache.regexp.internal.RE;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.io.PrintStream;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class ResultList {
    ArrayList<Result> resultList = new ArrayList<>();


    // Viser top-5 for given disciplin over en given distance
    public void showTopResultList(Control control, int answer, int answer1) {

        String disciplin = "";
        int distance = 0;
        if (answer == 1) {
            disciplin = "crawl";
        } else if (answer == 2) {
            disciplin = "rygsvømning";
        } else if (answer == 3) {
            disciplin = "butterfly";
        } else if (answer == 4) {
            disciplin = "hundesvømning";
        }
        if (answer1 == 1) {
            distance = 50;
        } else if (answer1 == 2) {
            distance = 100;
        } else if (answer1 == 3) {
            distance = 200;
        } else if (answer1 == 4) {
            distance = 400;
        }

        ArrayList<Result> tempResultList = new ArrayList<>();
        for (Result i : resultList) {
            if (i.getDisciplin().equalsIgnoreCase(disciplin) && i.getDistance() == distance) {
                tempResultList.add(i);
            }
        }

        Collections.sort(tempResultList, new Comparator<Result>() {
            @Override
            public int compare(Result o1, Result o2) {
                return Double.compare(o1.getResultTime(), o2.getResultTime());
            }
        });

        int count = 0;
        for (Result i : tempResultList) {

            if (count <= 5) {
                showSpecificResults(control, i.getResultID());
                count++;
            }
            if (count == 0) {
                System.out.println("Der findes ingen resultater i " + distance + " " + disciplin);
            }
        }
    }

    //Viser liste med ét eller flere medlemmers resultater
    //Modtager en kommasepareret tekststreng med medlemsnumre og viser disse/dette medlems resultater
        public void showSpecificResults (Control control, String results){
            String[] parts = results.split(",");
            for (Result i : resultList) {
                if (Arrays.asList(parts).contains(String.valueOf(i.getMemberID()))) {
                    System.out.printf("%-7d %-10d %-30s %-15s %-10d %-15s %-15s\n",
                            i.getResultID(), i.getMemberID(), i.getDate(), i.getDisciplin(), i.getDistance(), new DecimalFormat("#.##").format(i.getResultTime()), i.getLocation());
                }
            }
        }

        // Viser liste med ét medlems resulter
        public void showSpecificResults (Control control, Integer getResult){
            for (Result i : resultList) {
                if (getResult == i.getResultID()) {
                    System.out.printf("%-7d %-10d %-30s %-15s %-10d %-15s %-15s\n",
                            i.getResultID(), i.getMemberID(), i.getDate(), i.getDisciplin(), i.getDistance(), new DecimalFormat("#.##").format(i.getResultTime()), i.getLocation());
                }
            }
        }

        public void createNewResult (int medlem, String lokation, Date dato, String disciplin,int distance,
        double resultTime, Control control) throws FileNotFoundException {
            int resultNumber = createResultNumber();
            resultList.add(new Result(resultNumber, medlem, lokation, dato, disciplin, distance, resultTime));
            updateResultList(control);
        }

        public int createResultNumber () throws FileNotFoundException {
            Scanner scanner = new Scanner(new File("resultList.txt"));
            int results = 1;
            while (scanner.hasNext()) {
                for (int i = 1; i <= 7; i++) {
                    if (scanner.hasNext()) {
                        scanner.next();
                    }
                }
                results++;
            }
            return results;
        }

        public void updateResultList (Control control) throws FileNotFoundException {
            PrintStream printStream = new PrintStream("resultList.txt");

            for (Result i : resultList) {
                String date = control.financialTransactionList.sdf.format(i.getDate());
                printStream.println(i.getResultID() + "\n" + i.getMemberID() + "\n" + date + "\n" + i.getLocation()
                        + "\n" + i.getDistance() + "\n" + i.getDisciplin() + "\n" + i.getResultTime());

            }

        }

        public void loadResultList (Control control) throws FileNotFoundException, ParseException {
            Scanner scanner = new Scanner(new File("resultList.txt"));

            int MemberNumber;
            int ResultID;
            int Distance;
            String Location;
            String Disciplin;
            Date date;
            DateFormat dateFormat = control.financialTransactionList.sdf;
            double resultTime;

            while (scanner.hasNext()) {
                ResultID = Integer.parseInt(scanner.next());
                MemberNumber = Integer.parseInt(scanner.next());
                date = dateFormat.parse(scanner.next() + " " + scanner.next());
                Location = scanner.next();
                Distance = Integer.parseInt(scanner.next());
                Disciplin = scanner.next();
                resultTime = Double.parseDouble(scanner.next());
                resultList.add(new Result(ResultID, MemberNumber, Location, date, Disciplin, Distance, resultTime));
            }
        }

}


