# Svømmeklubben Delfinen #

Følg vejledningen for at få programmet op at køre.

### Eksamens opgave til 1 Semester på Datamatiker studiet KEA 2017 ###

Projekt start: D. 13 November 2017
Projekt aflevering: D. 07 December 2017

* Jonas S
* Kasper B.H.
* Mikkel D.N.
* Ludvig A.R.A
* Morten T

### Hvordan får jeg det op at køre? ###

* Download eller clone projektet.
* Åben din foretrukne IDE.
* Åben projektet i din IDE.
* Gå til projekt indstillinger i din IDE.
* JDK skal være 1.8
* "Prject language level" skal være 8.
* Vælg en compiler mappe.
* Vælg 'src' mappen som source.
* Nu skulle projektet gerne køre og compile for dig.

### Contribution guidelines ###

* Du er velkommen til at hente en kopi af projektet.
* Vi modtager dog ingen, ud over holdmedlemmernes, contributioner da det er et eksamems projekt. 

### Hvem skal jeg kontakte? ###

* Kontakt ejeren af dette repository for at få hjælp.